#include "HighArray.h"

#include<iostream>

void binarySearchUI( HighArray firstArray);

int main (int argc, char** argv) {

	HighArray firstArray;
	std::cout << "Start programm. Was added some elements in the not sorted array.";
	firstArray.insert(5);
	firstArray.insert(0);
	firstArray.insert(5);
	firstArray.insert(6);
	firstArray.insert(7);
	firstArray.insert(3);
	firstArray.insert(4);
	firstArray.insert(1);
	firstArray.insert(8);
	firstArray.insert(23);
	firstArray.insert(99);
	firstArray.insert(24);
	firstArray.insert(24);
	firstArray.insert(24);
	firstArray.insert(24);
	firstArray.insert(100);

	bool isArraySorted = false;
	for ( bool isExitRequested = false; !isExitRequested; ){
		std::cout << "\n\n1. Show array\n";
		std::cout << "2. Sort array\n";
		std::cout << "3. Binary search\n";
		std::cout << "4. Insert new value\n";
		std::cout << "5. Delete element\n";
		std::cout << "6. Delete maxValue\n";
		std::cout << "7. No dubs\n";
		std::cout << "8. Merge\n";
		std::cout << "9. Exit\n";
		std::cout << "Please, choise action: ";

		int action;
		std::cin >> action;
		switch (action) {
		case 1:
			firstArray.display();
			break;
		case 2:
			firstArray.sortArray();
			isArraySorted = true;
			std::cout << "Array sorted";
			break;
		case 3:
			if( isArraySorted){
				binarySearchUI( firstArray);
			} else{
				std::cout << "Sorry, array not sorted. Please sort array first";
			}
			break;
		case 4:
			std::cout << "Please, enter new value: ";
			int newValue;
			std::cin >> newValue;
			if( newValue > 0){
				if( isArraySorted){
					firstArray.insertInToSortedArray( newValue);
				} else{
					firstArray.insert( newValue);
				}
			} else {
				std::cout << "Incorrect value. Value should be positive";
			}
			break;
		case 5:
			std::cout << "Please, enter new value: ";
			int newValueForDelete;
			std::cin >> newValueForDelete;
			if( newValueForDelete > 0){
				if( isArraySorted){
					bool isDeleted = firstArray.delElementInSortedArray( newValueForDelete);
					std::cout << "Element is deleted: " << isDeleted << "\n";
				} else{
					std::cout << "Array is not sorted\n";
					bool isDeleted = firstArray.delElement( newValueForDelete);
					std::cout << "Element is deleted: " << isDeleted << "\n";
				}
			} else {
				std::cout << "Incorrect value. Value should be positive";
			}
			break;
		case 6:
			if( isArraySorted){
				bool isDeleted = firstArray.delMaxElementInSortedArray();
				std::cout << "Element is deleted: " << isDeleted << "\n";
			} else{
				std::cout << "Array is not sorted\n";
				int maxValue = firstArray.getMax();
				bool isDeleted = firstArray.delElement( maxValue);
				std::cout << "Element is deleted: " << isDeleted << "\n";
			}
			break;
		case 7:
			if( isArraySorted){
				std::cout << "Please, enter value for delete dubs: ";
				int valueDubs;
				std::cin >> valueDubs;
				if( valueDubs >= 0){
					bool dubsWasDeleted = firstArray.noDubs( valueDubs);
				} else{
					std::cout << "Value should be positive\n";
				}
			} else{
				std::cout << "Please, sort the array\n";
			}
			break;
		case 8:
		{
			std::cout << "Merge\n";

			HighArray secondArray;
			HighArray mergedArray;

			secondArray.insert(0);
			secondArray.insert(3);
			secondArray.insert(1);
			secondArray.insert(8);
			secondArray.insert(6);
			secondArray.insert(0);
			secondArray.insert(3);
			secondArray.insert(1);
			secondArray.insert(8);
			secondArray.insert(6);

			firstArray.sortArray();
			secondArray.sortArray();

			firstArray.display();
			secondArray.display();

			bool exitFromCycle = false;

			int countOfElementsInFirstArray = firstArray.getCountElements();
			int countOfElementsInSecondArray = secondArray.getCountElements();

			int currentIndexForFirstArray = 0;
			int currentIndexForSecondArray = 0;

			while( !exitFromCycle){
				int elementFromFirstArrayByIndex = firstArray.getElementFromArrayByIndex( currentIndexForFirstArray);
				int elementFromSecondArrayByIndex = secondArray.getElementFromArrayByIndex( currentIndexForSecondArray);

				if( elementFromFirstArrayByIndex > elementFromSecondArrayByIndex) {
					mergedArray.insert( elementFromSecondArrayByIndex);
					currentIndexForSecondArray++;
				} else if( elementFromFirstArrayByIndex < elementFromSecondArrayByIndex) {
					mergedArray.insert( elementFromFirstArrayByIndex);
					currentIndexForFirstArray++;
				} else if( elementFromFirstArrayByIndex == elementFromSecondArrayByIndex){
					mergedArray.insert( elementFromFirstArrayByIndex);
					mergedArray.insert( elementFromSecondArrayByIndex);
					currentIndexForFirstArray++;
					currentIndexForSecondArray++;
				}

				if( (countOfElementsInFirstArray == currentIndexForFirstArray)
				&& (currentIndexForSecondArray < countOfElementsInSecondArray) ){
					while( currentIndexForSecondArray != countOfElementsInSecondArray){
						elementFromSecondArrayByIndex = secondArray.getElementFromArrayByIndex( currentIndexForSecondArray);
						mergedArray.insert( elementFromSecondArrayByIndex);
						currentIndexForSecondArray++;
					}
					exitFromCycle = true;
				}

				if( (countOfElementsInFirstArray < currentIndexForFirstArray)
				&& (currentIndexForSecondArray == countOfElementsInSecondArray) ){
					while( currentIndexForFirstArray != countOfElementsInFirstArray){
						elementFromFirstArrayByIndex = firstArray.getElementFromArrayByIndex( currentIndexForFirstArray);
						mergedArray.insert( elementFromFirstArrayByIndex);
						currentIndexForFirstArray++;
					}
					exitFromCycle = true;
				}
			}
			mergedArray.display();
	}
		break;
		case 9:
			isExitRequested = true;
			break;
		default:
			std::cout << "Incorrect choise";
			break;
		}
	}
	return 0;
}

void binarySearchUI( HighArray firstArray){
	std::cout << "Please, enter search value: ";
	int searchValue;
	std::cin >> searchValue;
	if( searchValue >= 0){
		int searchResult = firstArray.binarySearch( searchValue);
		if( searchResult != -1) {
			std::cout<< "Index of element: " << searchResult;
		} else {
			std::cout << "Element not found";
		}
	} else{
		std::cout << "Value should be positive";
	}
}
