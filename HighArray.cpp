#include "HighArray.h"

#include <iostream>


HighArray::HighArray() {
	std::cout << "\nОбычный конструктор\n";
	this->m_arraySize = 1;
	this->m_nElems = 0;
	this->m_array = new int[this->m_arraySize];
}

HighArray::HighArray( const HighArray &obj){
	std::cout << "\nКопии конструктор\n";
	this->m_arraySize = obj.m_arraySize;
	this->m_nElems = obj.m_nElems;
	this->m_array = new int[this->m_arraySize];
	for( int i = 0; i < this->m_nElems; i++){
		this->m_array[ i] = obj.m_array[ i];
	}
}

HighArray::~HighArray(){
	std::cout << "\nДеструктор\n";
	delete [] this->m_array;
}

int HighArray::getCountElements(){
	return this->m_nElems;
}

void HighArray::insert( int value){
	if( this->m_nElems == 0){
		this->m_array[this->m_nElems] = value;
		this->m_nElems++;
		this->m_arraySize++;
	} else{
		int tempArray[this->m_nElems];
		for( int i = 0; i < this->m_nElems; i++){
			tempArray[ i] = this->m_array[ i];
		}

		delete [] this->m_array;

		this->m_array = new int[ this->m_arraySize];

		for( int i = 0; i < this->m_nElems; i++){
			this->m_array[ i] = tempArray[ i];
		}

		this->m_array[this->m_nElems] = value;
		this->m_nElems++;
		this->m_arraySize++;
	}
}

void HighArray::display(){
	for( int i = 0; i < this->m_nElems; i++){
		std::cout << this->m_array[i] << " ";
	}
	std::cout << "\n";
}

bool HighArray::find( int searchVal){
	bool isPresent = false;

	for( int i = 0; i < this->m_nElems; i++){
		if( this->m_array[i] == searchVal){
			isPresent = true;
		}
	}

	return isPresent;
}

bool HighArray::delElement( int value){
	bool isDeleteSuccess = false;
	int i;
	for( i = 0; i < this->m_nElems; i++){
		if( this->m_array[i] == value){
			isDeleteSuccess = true;
			break;
		}
	}
	if( isDeleteSuccess == true){
		for( int k = i; k < this->m_nElems; k++){
			this->m_array[k] = this->m_array[k+1];
		}
		this->m_nElems--;
		this->m_arraySize --;

		int tempArray[this->m_nElems] = {0};
		for( int i = 0; i < this->m_nElems; i++){
			tempArray[ i] = this->m_array[ i];
		}

		delete [] this->m_array;

		this->m_array = new int[ this->m_arraySize];

		for( int i = 0; i < this->m_nElems; i++){
			this->m_array[ i] = tempArray[ i];
		}
	}
	return isDeleteSuccess;
}

int HighArray::getMax(){

	int maxValue = 0;
	if( this->m_nElems == 0){
		maxValue = -1;
	} else{
		for( int i = 0; i < this->m_nElems; i++){
			if( maxValue < this->m_array[i]){
				maxValue = this->m_array[i];
			}
		}
	}
	return maxValue;
}

void HighArray::sortArray(){
	int massSize = this->m_nElems;
	while( massSize != 0){
		int maxValue = 0;
		for( int a = 0; a < massSize; a++){
			if( maxValue <= this->m_array[ a]){
				maxValue = this->m_array[ a];
			}
		}
		massSize --;
		for( int j = 0; j < massSize; j++){
			if( this->m_array[ j] == maxValue){
				for( int k = j; k < massSize; k++){
					this->m_array[ k] = this->m_array[ k+1];
					if( k+1 == massSize){
						break;
					}
				}
				this->m_array[ massSize] = maxValue;
			}
		}
	}
}

int HighArray::binarySearch( int searchValue){
	int lowerLevel = 0;
	int upperLevel = this->m_nElems;
	int midLevel;
	int indexOfElement = -1;
	bool isExit = false;
	while(!isExit){
		midLevel = (lowerLevel + upperLevel) / 2;
		if( this->m_array[midLevel] == searchValue){
			isExit = true;
			indexOfElement = midLevel;
		} else if( midLevel == lowerLevel || midLevel == upperLevel) {
			isExit = true;
		} else {
			if( searchValue > this->m_array[ midLevel]){
				lowerLevel = midLevel;
			} else {
				upperLevel = midLevel;
			}
		}
	}
	return indexOfElement;
}

/*
 * Differens betwen first and second part is: at first part we know index for element (because we know
 * that same value is in array. At second part we doen't know index and we can figure out him.
*/
void HighArray::insertInToSortedArray( int value){
	int dubs = seachDubs( value);
	int indexOfElement = -1;
	if( dubs > 0) {
		indexOfElement = binarySearch( value);
		if( indexOfElement != -1) {
			int tempArray[ this->m_nElems];
			for( int i = 0; i < this->m_nElems; i++){
				tempArray[ i] = this->m_array[ i];
			}
			delete[] this->m_array;

			this->m_nElems++;
			this->m_arraySize++;

			this->m_array = new int[ this->m_arraySize];
			bool isInserted = false;
			for( int i = 0; i < this->m_nElems; i++){
				if( i == indexOfElement){
					this->m_array[ i] = value;
					isInserted = true;
				} else{
					if( isInserted){
						this->m_array[ i] = tempArray[ i-1];
					} else{
						this->m_array[ i] = tempArray[ i];
					}
				}
			}
		}
	} else {
		int lowerLevel = 0;
		int upperLevel = this->m_nElems;
		int midLevel;
		bool isExit = false;
		while(!isExit){
			midLevel = (lowerLevel + upperLevel) / 2;
			if( this->m_array[midLevel] < value && this->m_array[ midLevel+1] > value ){
				isExit = true;
				indexOfElement = midLevel;
			} else if( midLevel == lowerLevel || midLevel == upperLevel) {
				isExit = true;
			} else {
				if( value > this->m_array[ midLevel]){
					lowerLevel = midLevel;
				} else {
					upperLevel = midLevel;
				}
			}
		}
		int tempArray[ this->m_nElems];
		for( int i = 0; i < this->m_nElems; i++){
			tempArray[ i] = this->m_array[ i];
		}
		delete[] this->m_array;

		this->m_nElems++;
		this->m_arraySize++;

		this->m_array = new int[ this->m_arraySize];
		bool isInserted = false;
		for( int i = 0; i < this->m_nElems; i++){
			if( i == indexOfElement + 1){
				this->m_array[ i] = value;
				isInserted = true;
			} else{
				if( isInserted){
					this->m_array[ i] = tempArray[ i-1];
				} else{
					this->m_array[ i] = tempArray[ i];
				}
			}
		}
	}
}

bool HighArray::delElementInSortedArray( int value){
	bool isDeleteSuccess = false;
	int indexOfElement = binarySearch( value);

	if( this->m_array[ indexOfElement] == value){
		isDeleteSuccess = true;
	}
	if( isDeleteSuccess){
		for( int k = indexOfElement; k < this->m_nElems; k++){
			this->m_array[k] = this->m_array[k+1];
		}
		this->m_nElems--;
		this->m_arraySize --;

		int tempArray[this->m_nElems] = {0};
		for( int i = 0; i < this->m_nElems; i++){
			tempArray[ i] = this->m_array[ i];
		}

		delete [] this->m_array;

		this->m_array = new int[ this->m_arraySize];

		for( int i = 0; i < this->m_nElems; i++){
			this->m_array[ i] = tempArray[ i];
		}
	}
	return isDeleteSuccess;
}

bool HighArray::delMaxElementInSortedArray( ){
	int maxValue = getMax();
	bool isDeleteSuccess = delElementInSortedArray( maxValue);
	return isDeleteSuccess;
}

int HighArray::seachDubs( int value){
	int count = 0;
	for( int i = 0; i < this->m_nElems; i++){
		if( this->m_array[ i] == value){
			count++;
		}
	}
	return count;
}

bool HighArray::noDubs( int value){
	int countOfDubs = seachDubs( value);
	bool dubsWasDeleted = false;
	if( countOfDubs > 1){
		while( countOfDubs !=1){
			delElementInSortedArray( value);
			countOfDubs --;
		}
	} else{
		std::cout << "No dubs found\n";
	}
	return dubsWasDeleted;
}

int HighArray::getElementFromArrayByIndex( int index){
	return this->m_array[ index];
}
