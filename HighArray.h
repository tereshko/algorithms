#include <iostream>

#ifndef H_HIGHARRAY
#define H_HIGHARRAY

class HighArray {

public:
	HighArray();
	HighArray( const HighArray &obj);
	~HighArray();
	void insert( int value);
	void display();
	bool find( int searchKey);
	bool delElement( int value);
	int getMax();
	void sortArray();
	int binarySearch( int searchValue);
	void binarySearchElementUI();
	void insertInToSortedArray( int value);
	int seachDubs( int value);
	bool delElementInSortedArray( int value);
	bool delMaxElementInSortedArray();
	bool noDubs( int value);
	int getCountElements();
	int getElementFromArrayByIndex( int index);
private:
	int* m_array;
	int m_nElems;
	size_t m_arraySize;
};
#endif